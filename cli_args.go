package main

import (
	"log"

	"github.com/alexflint/go-arg"
)

type VerbItems struct {
	Url  string `arg:"positional" default:"-"`
	File string `arg:"positional" default:"-"`
}

type Args struct {
	LocDescs    *VerbItems `arg:"subcommand:loc-descs" help:"Pull a location description JSON and parse to csv"`
	Multipliers *VerbItems `arg:"subcommand:multipliers" help:"Pull a meter data JSON and parse to csv"`
	ServTypes   *VerbItems `arg:"subcommand:service-types" help:"Pull a service type JSON and parse to csv"`
}

type Configuration struct {
	Items         VerbItems
	IsLocDescs    bool
	IsMultipliers bool
	IsServTypes   bool
}

func (opts *VerbItems) import_opts(cl_opts *VerbItems) {
	if cl_opts.Url == "-" {
		log.Fatal("You must supply an URL")
	}

	opts.File = cl_opts.File
	opts.Url = cl_opts.Url
}

func parse_args() Configuration {
	args := Args{}
	opts := Configuration{
		Items:         VerbItems{},
		IsLocDescs:    false,
		IsMultipliers: false,
		IsServTypes:   false,
	}

	arg.MustParse(&args)

	switch {
	case args.LocDescs != nil:
		opts.Items.import_opts(args.LocDescs)
		opts.IsLocDescs = true
	case args.Multipliers != nil:
		opts.Items.import_opts(args.Multipliers)
		opts.IsLocDescs = true
	case args.ServTypes != nil:
		opts.Items.import_opts(args.ServTypes)
		opts.IsServTypes = true
	}

	return opts
}
