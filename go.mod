module blackriver/csv-dumper

// ¯\_(ツ)_/¯

go 1.20

require (
	github.com/alexflint/go-arg v1.4.3 // indirect
	github.com/alexflint/go-scalar v1.1.0 // indirect
	github.com/iancoleman/strcase v0.1.3 // indirect
	go.ectobit.com/clap v0.2.3 // indirect
)
