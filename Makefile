DEST=target
OUTPUT=$(DEST)/csv-dumper

all: $(OUTPUT)

clean:
	rm -rfv $(DEST)

run: all
	$(OUTPUT)

$(OUTPUT): *.go
	@mkdir -p $(DEST)
	go build -o $@ $^
