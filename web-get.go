package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"
)

func process_json[RowType ContainerType](url string, output chan []string) {
	defer close(output)

	rows := make(chan RowType)
	go get_json(url, rows)

	for row := range rows {
		string_row := row.to_string_array()
		output <- string_row
	}
}

func get_json[RowType any](url string, output chan RowType) {
	defer close(output)

	httpClient := http.Client{
		Timeout: time.Second * 30,
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	fatal_err(err)

	res, err := httpClient.Do(req)
	fatal_err(err)

	container := []RowType{}
	if res.Body != nil {
		defer res.Body.Close()

		body, err := io.ReadAll(res.Body)
		fatal_err(err)

		err = json.Unmarshal(body, &container)
		fatal_err(err)
	} else {
		log.Fatal("Empty body")
	}

	for _, e := range container {
		output <- e
	}
}
